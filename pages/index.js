import Script from "next/script";

export default function Home() {
  function pay() {
    t05pay(
      {
        accessToken: "<CHANGE_THIS_TOKEN>", // Please change this
        amountInCents: 100, // for $1 = 100cents
        customOrderId: "MyCustomOrder",
      },
      function (res) {
        console.log("t05pay failed", res); // Callback function - Used to handle failure
      }
    );
  }
  return (
    <div className="container">
      <main>
        <h1 className="title">
          Welcome to <a href="https://nextjs.org">Next.js!</a>
        </h1>
        <button onClick={pay}>Test Pay</button>
      </main>

      <Script src="secure_pay_staging.min.js" strategy="beforeInteractive" />
    </div>
  );
}
